﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using SeoChecker.Models;
using System.Web;
using System.Windows.Forms;
using SeoChecker.Extensions;
using SeoChecker.Integrations;
using SeoChecker.Models.DomElements;
using SeoChecker.RestSharp;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;
using System.Drawing;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using SeoChecker.Constants;
using SeoChecker.Helpers;
using SeoChecker.Integrations.Models;
using SeoChecker.Models.SeleniumModels;

namespace SeoChecker
{
    public class SeoChecker
    {
        private readonly string _url;
        private readonly HtmlWeb _web;
        private readonly HtmlDocument _doc;
        private readonly EndpointChecker _endpointChecker;

 
        public SeoChecker(string url)
        {

            url = EndpointChecker.GetTrueUrl(url);
            _url = url;
            _web = new HtmlWeb();
            _doc = _web.Load(url);
            _endpointChecker = new EndpointChecker(url);

        }

        public SpeedOptimization GetSpeedOptimization()
        {
            var size = _endpointChecker.GetContentSize(_url);
            var sizeInKb = decimal.Round(decimal.Divide(size, 1024), 2);
            var googlePageSpeed = GooglePageSpeedConsumer.GetPageSpeed(_url);
            var pageSpeed = new GooglePageSpeed()
            {
                GZipEnabled = googlePageSpeed.FormattedResults.RuleResults.EnableGzipCompression.RuleImpact == 0,
                LargeImagesAndPossibleSave = googlePageSpeed.FormattedResults.RuleResults.OptimizeImages.UrlBlocks?
                    .SelectMany(x => x.Urls)
                    .Select(x => x.result)
                    .ToDictionary(o => o.Args[0].Value, o => o.Args[1].Value + $"({o.Args[2].Value})"),
                Score = googlePageSpeed.RuleGroups.SPEED.score,
                UncachedRessources = googlePageSpeed.FormattedResults.RuleResults.LeverageBrowserCaching.UrlBlocks?
                    .SelectMany(x => x.Urls)
                    .Select(x => x.result)
                    .Select(x => x.Args[0].Value).ToList(),
                UnminifiedCss = googlePageSpeed.FormattedResults.RuleResults.MinifyCss.UrlBlocks?
                    .SelectMany(x => x.Urls)
                    .Select(x => x.result)
                    .Select(x => x.Args[0].Value).ToList(),
                UnminifiedJavascript = googlePageSpeed.FormattedResults.RuleResults.MinifyJavaScript.UrlBlocks?
                    .SelectMany(x => x.Urls)
                    .Select(x => x.result)
                    .Select(x => x.Args[0].Value).ToList(),

            };

            return new SpeedOptimization()
            {
                HtmlSizeInKb = sizeInKb,
                GooglePageSpeed = pageSpeed
            };
        }

        public List<string> GetCommonKeywords()
        {
            var nodes = _doc.DocumentNode.SelectNodes("//p");
            var allWords = new List<string>();
            foreach (var node in nodes)
            {
                allWords.AddRange(node.InnerText.ToLower().Replace(".", "").Replace(",", "").Split(' ').ToList());
            }

            return allWords.Where(x => x.Length > 3).GroupBy(x => x).OrderByDescending(g => g.Count())
                .Select(x => x.Key).Take(5).ToList();
        }

        public Meta GetMeta()
        {
            var meta = new Meta
            {
                Description = HttpUtility.HtmlDecode(_doc.DocumentNode.SelectSingleNode("//meta[@name='description']")?.GetAttributeValue("content") ?? _doc.DocumentNode.SelectSingleNode("//meta[@name='Description']")?.GetAttributeValue("content")),
                Robots = _doc.DocumentNode.SelectSingleNode("//meta[@name='robots']")?.GetAttributeValue("content"),
                Title = System.Web.HttpUtility.HtmlDecode(_doc.DocumentNode.SelectSingleNode("//title")?.InnerHtml),
                OpenGraphImageUrl = _doc.DocumentNode.SelectSingleNode("//meta[@name='og:image']")?.GetAttributeValue("content") ?? _doc.DocumentNode.SelectSingleNode("//meta[@property='og:image']")?.GetAttributeValue("content"),
                OpenGraphDescription = _doc.DocumentNode.SelectSingleNode("//meta[@name='og:description']")?.GetAttributeValue("content") ?? _doc.DocumentNode.SelectSingleNode("//meta[@property='og:description']")?.GetAttributeValue("content"),
                OpenGraphUrl = _doc.DocumentNode.SelectSingleNode("//meta[@name='og:url']")?.GetAttributeValue("content") ?? _doc.DocumentNode.SelectSingleNode("//meta[@property='og:url']")?.GetAttributeValue("content"),
                OpenGraphTitle = _doc.DocumentNode.SelectSingleNode("//meta[@name='og:title']")?.GetAttributeValue("content") ?? _doc.DocumentNode.SelectSingleNode("//meta[@property='og:url']")?.GetAttributeValue("content"),
                Canonical = _doc.DocumentNode.SelectSingleNode("//link[@name='canonical']")?.GetAttributeValue("href"),
            };

            return meta;
        }

        public MobileOptimization GetMobileOptimization()
        {
            var path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
            IWebDriver driver = new ChromeDriver(path);
            var javascriptExecutor = (IJavaScriptExecutor)driver;

            driver.Manage().Window.Size = new Size(410, 800);



            driver.Navigate().GoToUrl(_url);

            var navigationStart = Convert.ToInt64(javascriptExecutor.ExecuteScript("return window.performance.timing.navigationStart"));
            var responseStart = Convert.ToInt64(javascriptExecutor.ExecuteScript("return window.performance.timing.responseStart"));
            var domComplete = Convert.ToInt64(javascriptExecutor.ExecuteScript("return window.performance.timing.domComplete"));

            var backendPerformance = responseStart - navigationStart;
            var frontendPerformance = domComplete - responseStart;

            var time = new TimeSpan(0, 0, 0, 0, (int)frontendPerformance);
            //Mobile screenshot
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();

            //Check if page has scrollbar on x Axis, and therefor is not mobile optimized
            var hasXScrollBar = (bool)javascriptExecutor.ExecuteScript("var x = document.getElementsByTagName('body')[0];var hasHorizontalScrollbar = x.scrollWidth > x.clientWidth + 20;return hasHorizontalScrollbar;");

            driver.Quit();

            var model = new MobileOptimization();

            model.PageImageBase64 = ss.AsBase64EncodedString;
            model.IsMobileOptimized = !hasXScrollBar;
            model.PageLoadSpeed = time;



            return model;
        }

        public PageContent GetPageContents()
        {

            var path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
            IWebDriver driver = new ChromeDriver(path);
            var javascriptExecutor = (IJavaScriptExecutor)driver;

            driver.Manage().Window.Size = new Size(410, 800);



            driver.Navigate().GoToUrl(_url);


            String scriptToExecute = "var performance = window.performance || window.mozPerformance || window.msPerformance || window.webkitPerformance || {}; var network = performance.getEntries() || {}; return network;";
            var obj = javascriptExecutor.ExecuteScript(scriptToExecute);
            var str = JsonConvert.SerializeObject(obj);
            var networkItems = JsonConvert.DeserializeObject<List<NetworkTiming>>(str);


            var pageContents = new PageContent()
            {
                H1S = _doc.DocumentNode.Descendants("h1")?.Select(x => x?.InnerTextDecoded()).ToList(),
                H2S = _doc.DocumentNode.Descendants("h2")?.Select(x => x?.InnerTextDecoded()).ToList(),
                Links = _doc.DocumentNode.Descendants("a").Where(x => x.AnchorHasHref()).Select(x => new Anchor()
                {
                    FullAnchor = x.OuterHtml,
                    Href = x.GetAttributeValue("href"),
                    IsAlive = 
                        x.GetAttributeValue("href").ToLower().Contains("javascript:void(0)") || 
                        x.GetAttributeValue("href").ToLower().StartsWith("#") || 
                        x.GetAttributeValue("href").ToLower().StartsWith("tel:") || 
                        x.GetAttributeValue("href").ToLower().StartsWith("mailto:") || 
                        x.GetAttributeValue("href").ToLower().StartsWith("javascript:;") || 
                        _endpointChecker.IsEndpointAlive(x.GetAttributeValue("href"))
                }).ToList(),
                Images = _doc.DocumentNode.Descendants("img").Select(x => new Models.DomElements.Image()
                {
                    AltText = x.GetAttributeValue("alt"),
                    Src = x.GetAttributeValue("src"),
                    //Size = _endpointChecker.GetContentSize(x.GetAttributeValue("src"))
                }).ToList(),
                GoogleAnalyticsEnabled = _doc.DocumentNode.OuterHtml.Contains("GoogleAnalyticsObject") || _doc.DocumentNode.OuterHtml.ToLower().Contains("googletagmanager"),
                InlineCss = _doc.DocumentNode.SelectNodes("//*[@style]")?.Select(x => x.OuterHtml).ToList()
        };

            //favicon
            var favicon = _doc.DocumentNode.SelectSingleNode("//link[@rel='icon']") ??
                          _doc.DocumentNode.SelectSingleNode("//link[@rel='shortcut icon']");

            pageContents.FaviconInMeta = favicon != null;

            if (favicon != null)
            {
                var faviconUrl = favicon?.GetAttributeValue("href");
                var endpoint = faviconUrl.Contains("http") || faviconUrl.Contains("www") ? faviconUrl : _url + faviconUrl;
                pageContents.FaviconUrl =
                    _endpointChecker.GetContentSize(endpoint) > 0
                        ? endpoint
                        : null;
            }
            else
            {
                pageContents.FaviconUrl = _endpointChecker.GetContentSize("/favicon.ico") > 0 ? _url + "/favicon.ico" : null;
            }

            //CSS and Scripts
            var cssFileUrls = networkItems.Where(x => x.Type == "link" || (x.Type == "css" && x.Name.ToLower().Contains(".css"))).Select(x => x.Name).ToList();
            var scriptFileUrls = networkItems.Where(x => x.Type == "script").Select(x => x.Name).ToList();
            var htmlFiles = networkItems.Where(x => x.Type == "navigation").Select(x => x.Name).ToList();
            var imageCount = networkItems.Count(x => x.Type == "img" || (x.Type == "css" && !Helpers.Helpers.IsStringFontRef(x.Name)));

            var cssFiles = cssFileUrls.Select(x => new CssFile()
            {
                IsInternal = !x.Contains("http") || x.Contains(_url),
                Path = x
            }).ToList();

            var scriptFiles = scriptFileUrls.Select(x => new ScriptFile()
            {
                IsInternal = !x.Contains("http") || x.Contains(_url),
                Path = x
            }).ToList();

            pageContents.CssFiles = cssFiles;
            pageContents.ScriptFiles = scriptFiles;
            pageContents.HtmlFiles = htmlFiles.Select(x => new HtmlFile(){Url = x}).ToList();
            pageContents.TotalImageDownloadCount = imageCount;

            return pageContents;
        }

        public Extras GetExtras()
        {
            var robotsUrl = $"{_url}/robots.txt";
            var sitemapUrl = $"{_url}/sitemap.xml";


            var robotsContents = "";
            try
            {
                robotsContents = Robot.ParseRobotsTxtFile(_url);
            }
            catch (Exception e)
            {
                
            }

            var extras = new Extras()
            {
                RobotsTxtUrl = !string.IsNullOrEmpty(robotsContents) ? robotsUrl : null,
                RotoctsTxtContents = robotsContents,
                SitemapXmlUrl = _endpointChecker.IsEndpointAlive(sitemapUrl) ? sitemapUrl : null
            };

            return extras;
        }

        public Security GetSecurity()
        {
            var security = new Security()
            {
                IsHttps = _url.Substring(0,5).ToLower().Contains("https"),
                CertificateValid = _endpointChecker.certificateIsValid,
                VirusReport = VirusTotalApiConsumer.GetVirusReport(_url)
            };

            return security;
        }


    }
}