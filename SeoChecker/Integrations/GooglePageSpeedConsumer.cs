﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using SeoChecker.Integrations.Models;
using SeoChecker.Integrations.Models.google;

namespace SeoChecker.Integrations
{
    public class GooglePageSpeedConsumer
    {
        public static GoogleMaster GetPageSpeed(string url)
        {


            var client = new RestClient($"https://www.googleapis.com/pagespeedonline/v4/runPagespeed?url={url}&strategy=mobile&key=AIzaSyAVDTH2E5O2yieujeYgo82PhXsVpiPn5NY");

            client.AddDefaultHeader("Content-Type", "application/json");
            client.AddDefaultHeader("Accept", "application/json");


            var request = new RestRequest(Method.GET);

            var response = client.Execute<GoogleMaster>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Data;
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException();
            }
            else if (response.StatusCode == HttpStatusCode.RequestTimeout ||
                     response.StatusCode == HttpStatusCode.GatewayTimeout || response.ErrorMessage == "The operation has timed out" || response.StatusCode == 0 || response.ErrorMessage == "Handlingen blev afbrudt pga. timeout")
            {
                throw new TimeoutException();
            }

            throw new Exception("API Error");
        }
    }
}
