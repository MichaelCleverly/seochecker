﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Integrations.Models
{
    [DataContract]
    public class VirusReport
    {
        [DataMember(Name = "total")]
        public int Total { get; set; }

        [DataMember(Name= "positives")]
        public int Positives { get; set; }
    }
}
