﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Integrations.Models
{
    public class GooglePageSpeed
    {
        public int Score { get; set; }
        public bool GZipEnabled { get; set; }
        public List<string> UncachedRessources { get; set; }
        public List<string> UnminifiedJavascript { get; set; }
        public List<string> UnminifiedCss { get; set; }
        public Dictionary<string, string> LargeImagesAndPossibleSave { get; set; }

    }
}
