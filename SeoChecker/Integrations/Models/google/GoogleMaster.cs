﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Integrations.Models.google
{
    [DataContract]
    public class GoogleMaster
    {
        [DataMember(Name="formattedResults")]
        public FormattedResults FormattedResults { get; set; }

        [DataMember(Name= "ruleGroups")]
        public RuleGroups RuleGroups { get; set; }
    }
}
