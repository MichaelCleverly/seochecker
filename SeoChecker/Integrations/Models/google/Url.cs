﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Integrations.Models.google
{
    [DataContract]
    public class Url
    {
        [DataMember(Name = "result")]
        public UrlResult result { get; set; }
    }
}
