﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Integrations.Models.google
{
    [DataContract]
    public class RuleResult
    {
        public Rule EnableGzipCompression { get; set; }
        public Rule MinifyCss { get; set; }
        public Rule MinifyHTML { get; set; }
        public Rule MinifyJavaScript { get; set; }
        public Rule OptimizeImages { get; set; }
        public Rule LeverageBrowserCaching { get; set; }


    }
}
