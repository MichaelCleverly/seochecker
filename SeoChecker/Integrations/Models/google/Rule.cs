﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Integrations.Models.google
{
    [DataContract]
    public class Rule
    {
        [DataMember(Name= "ruleImpact")]
        public int RuleImpact { get; set; }

        [DataMember(Name = "urlBlocks")]
        public List<UrlBlock> UrlBlocks { get; set; }

    }
}
