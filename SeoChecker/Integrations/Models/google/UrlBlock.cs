﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Integrations.Models.google
{
    [DataContract]
    public class UrlBlock
    {
        [DataMember(Name = "urls")]
        public List<Url> Urls { get; set; }
    }
}
