﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using SeoChecker.Integrations.Models;

namespace SeoChecker.Integrations
{
    public class VirusTotalApiConsumer
    {
        public static VirusReport GetVirusReport(string url)
        {


            var client = new RestClient($"https://www.virustotal.com/vtapi/v2/url/report?apikey=d33a9a74a205c61a11cc9761d375eb860a4654a197ba5cf0231a0c65d6e482c3&resource={url}");

            client.AddDefaultHeader("Content-Type", "application/json");
            client.AddDefaultHeader("Accept", "application/json");


            var request = new RestRequest(Method.GET);

            var response = client.Execute<VirusReport>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Data;
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException();
            }
            else if (response.StatusCode == HttpStatusCode.RequestTimeout ||
                     response.StatusCode == HttpStatusCode.GatewayTimeout || response.ErrorMessage == "The operation has timed out" || response.StatusCode == 0 || response.ErrorMessage == "Handlingen blev afbrudt pga. timeout")
            {
                throw new TimeoutException();
            }

            throw new Exception("API Error");
        }
    }
}
