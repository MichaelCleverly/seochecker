﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Models
{
    public class Meta
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Robots { get; set; }
        public string Canonical { get; set; }


        public string OpenGraphImageUrl { get; set; }
        public string OpenGraphUrl { get; set; }
        public string OpenGraphTitle { get; set; }
        public string OpenGraphDescription { get; set; }
    }
}
