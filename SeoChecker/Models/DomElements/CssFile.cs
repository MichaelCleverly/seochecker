﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Models.DomElements
{
    public class CssFile
    {
        public string Path { get; set; }
        public bool IsInternal { get; set; }
        public bool IsMinified { get; set; }
    }
}
