﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Models.DomElements
{
    public class Image
    {
        public string Src { get; set; }
        public string AltText { get; set; }
        public int Size { get; set; }
    }
}
