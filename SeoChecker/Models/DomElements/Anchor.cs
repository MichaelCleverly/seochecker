﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Models.DomElements
{
    public class Anchor
    {
        public string Href { get; set; }
        public bool IsAlive { get; set; }
        public string FullAnchor { get; set; }
    }
}
