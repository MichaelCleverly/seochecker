﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Models
{
    public class Extras
    {

        public string RobotsTxtUrl { get; set; }
        public string RotoctsTxtContents { get; set; }

        public string SitemapXmlUrl { get; set; }

    }
}
