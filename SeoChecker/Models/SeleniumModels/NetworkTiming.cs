﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Models.SeleniumModels
{
    [DataContract(Name= "PerformanceResourceTiming")]
    public class NetworkTiming
    {
        [DataMember(Name = "initiatorType")]
        public string Type { get; set; }
        [DataMember(Name= "name")]
        public string Name { get; set; }
    }
}
