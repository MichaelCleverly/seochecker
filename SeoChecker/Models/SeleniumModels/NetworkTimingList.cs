﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Models.SeleniumModels
{
    public class NetworkTimingList
    {
        public List<NetworkTiming> NetworkTimingModels { get; set; }
    }
}
