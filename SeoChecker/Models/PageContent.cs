﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeoChecker.Models.DomElements;

namespace SeoChecker.Models
{
    public class PageContent
    {
        public List<string> H1S { get; set; }
        public List<string> H2S { get; set; }
        public List<Anchor> Links { get; set; }
        public List<Image> Images { get; set; }
        public List<ScriptFile> ScriptFiles { get; set; }
        public List<CssFile> CssFiles { get; set; }
        public List<string> InlineCss { get; set; }
        public List<HtmlFile> HtmlFiles { get; set; }
        public int TotalImageDownloadCount { get; set; }
        public bool FaviconInMeta { get; set; }
        public string FaviconUrl { get; set; }
        public bool GoogleAnalyticsEnabled { get; set; }
    }
}
