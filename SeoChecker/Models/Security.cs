﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeoChecker.Integrations.Models;

namespace SeoChecker.Models
{
    public class Security
    {
        public bool IsHttps { get; set; }
        public bool CertificateValid { get; set; }
        public VirusReport VirusReport { get; set; }
    }
}
