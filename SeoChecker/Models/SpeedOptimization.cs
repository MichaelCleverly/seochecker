﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeoChecker.Integrations.Models;

namespace SeoChecker.Models
{
    public class SpeedOptimization
    {
        public decimal HtmlSizeInKb { get; set; }
        public GooglePageSpeed GooglePageSpeed { get; set; }
    }
}
