﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Models
{
    public class MobileOptimization
    {
        public TimeSpan PageLoadSpeed { get; set; }
        public string PageImageBase64 { get; set; }
        public bool IsMobileOptimized { get; set; }
    }
}
