﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Constants
{
    public class FontTypes
    {
        public static List<string> GetFontTypes()
        {
            var fonts = new List<string>();
            fonts.Add(".ABF");
                fonts.Add(".ACFM");
            fonts.Add(".AFM");
            fonts.Add(".AMFM");
            fonts.Add(".BDF");
            fonts.Add(".CHA");
            fonts.Add(".CHR");
            fonts.Add(".COMPOSITEFONT");
            fonts.Add(".DFONT");
            fonts.Add(".EOT");
            fonts.Add(".ETX");
            fonts.Add(".EUF");
            fonts.Add(".F3F");
            fonts.Add(".FFIL");
            fonts.Add(".FNT");
            fonts.Add(".FON");
            fonts.Add(".FOT");
            fonts.Add(".GDR");
            fonts.Add(".GF");
            fonts.Add(".GXF");
            fonts.Add(".LWFN");
            fonts.Add(".MCF");
            fonts.Add(".MF");
            fonts.Add(".MXF");
            fonts.Add(".NFTR");
            fonts.Add(".ODTTF");
            fonts.Add(".OTF");
            fonts.Add(".PCF");
            fonts.Add(".PFA");
            fonts.Add(".PFB");
            fonts.Add(".PFM");
            fonts.Add(".PFR");
            fonts.Add(".PK");
            fonts.Add(".PMT");
            fonts.Add(".SFD");
            fonts.Add(".SFP");
            fonts.Add(".SUIT");
            fonts.Add(".T65");
            fonts.Add(".TFM");
            fonts.Add(".TTC");
            fonts.Add(".TTE");
            fonts.Add(".TTF");
            fonts.Add(".TXF");
            fonts.Add(".VFB");
            fonts.Add(".VLW");
            fonts.Add(".VNF");
            fonts.Add(".WOFF");
            fonts.Add(".WOFF2");
            fonts.Add(".XFN");
            fonts.Add(".XFT");
            fonts.Add(".YTF");


            return fonts;
        }
    }
}
