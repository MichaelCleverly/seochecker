﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeoChecker.Extensions
{
    public static class StringExtensions
    {
        public static string ConvertFromIsoToUtf8(this string input)
        {
            return Encoding.GetEncoding("iso-8859-1").GetString(Encoding.UTF8.GetBytes(input));
        }
    }
}
