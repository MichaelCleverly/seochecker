﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace SeoChecker.Extensions
{
    public static class HtmlNodeExtensions
    {

        public static bool AnchorHasHref(this HtmlNode node)
        {
            return node.Attributes["href"] != null;
        }

        public static string GetAttributeValue(this HtmlNode node, string attr)
        {
            var attribute = node.Attributes[attr];
            string fullAttribute = attribute?.Value;
            return System.Web.HttpUtility.HtmlDecode(fullAttribute);
        }

        public static string InnerTextDecoded(this HtmlNode node)
        {
            return System.Web.HttpUtility.HtmlDecode(node.InnerText);
        }
    }
}
