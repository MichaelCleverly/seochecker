﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace SeoChecker.RestSharp
{
    public class EndpointChecker
    {
        private readonly string _baseUrl;
        public bool certificateIsValid;

        public EndpointChecker(string baseUrl)
        {
            _baseUrl = baseUrl;
            certificateIsValid = false;
            try
            {
                CheckCertificate();
            }
            catch (Exception e)
            {
                
            }

        }

        public bool IsEndpointAlive(string endpoint)
        {
            
            try
            {
                endpoint = endpoint.Contains("http") || endpoint.Contains("www") ? endpoint : _baseUrl + endpoint;

                var client = new RestClient(endpoint);
                client.AddDefaultHeader("Accept", "text/html,text/plain,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                var request = new RestRequest(endpoint, Method.GET);
                var response = client.Execute(request);
                return response.StatusCode == HttpStatusCode.OK || response.StatusCode == (HttpStatusCode)999;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public string GetTextFromUrl(string url)
        {
            url = url.Contains("http") || url.Contains("www") || url.StartsWith("//") ? url : _baseUrl + url;
            var webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Accept = "*/*";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                   | SecurityProtocolType.Tls11
                                                   | SecurityProtocolType.Tls12
                                                   | SecurityProtocolType.Ssl3;


            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            try
            {
                using (var response = webRequest.GetResponse())
                using (var content = response.GetResponseStream())
                    if (content != null)
                        using (var reader = new StreamReader(content))
                        {
                            var fullContents = reader.ReadToEnd();
                            return fullContents;
                        }

                return null;
            }
            catch (Exception e)
            {
           
                return "Kunne ikke få indhold";
            }

        }

        public string GetStringContent(string endpoint)
        {
            endpoint = endpoint.Contains("http") || endpoint.Contains("www") ? endpoint : _baseUrl + endpoint;

            var client = new RestClient(endpoint);
            client.AddDefaultHeader("Accept", "*/*");

            var request = new RestRequest(endpoint, Method.GET);
            request.AddHeader("Accept", "*/*");

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                   | SecurityProtocolType.Tls11
                                                   | SecurityProtocolType.Tls12
                                                   | SecurityProtocolType.Ssl3;


            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            var response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Content;
            }

            return null;
        }

        public int GetContentSize(string endpoint)
        {
            endpoint = endpoint.Contains("http") || endpoint.Contains("www") ? endpoint : _baseUrl + endpoint;

            var client = new RestClient(endpoint);
            client.AddDefaultHeader("Accept", "*/*");

            var request = new RestRequest(endpoint, Method.GET);
            request.AddHeader("Accept", "*/*");

            var response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.RawBytes.Length;
            }

            return 0;
        }



        public static string GetTrueUrl(string url)
        {
            if (!url.ToLower().Contains("http"))
            {
                url = "http://" + url;
            }
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            var client = new RestClient(url);
            client.AddDefaultHeader("User-Agent", "Mozilla 5.0; Revolo SEO-Checker;");
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                url = response.ResponseUri.AbsoluteUri.TrimEnd('/');
                return url;
            }

            throw new Exception("Url not valid");
        }

        private bool CheckCertificate()
        {
            HttpWebRequest request = WebRequest.CreateHttp(_baseUrl);
            request.ServerCertificateValidationCallback += ServerCertificateValidationCallback;
            using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
            {
                return true;
            }
        }

        private bool ServerCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
            {
                certificateIsValid = true;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
