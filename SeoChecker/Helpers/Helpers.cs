﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeoChecker.Constants;
using ServiceStack.Html;

namespace SeoChecker.Helpers
{
    public class Helpers
    {
        public static byte[] ImageToByte2(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public static bool IsCssMinified(string css)
        {
            var threshold = 1.10m;
            var compressedCss = Minifiers.Css.Compress(css);
            return compressedCss.Length * threshold > css.Length;

        }

        public static bool IsJsMinified(string js)
        {
            var threshold = 1.20m;
            var compressedJs = Minifiers.JavaScript.Compress(js);
            return compressedJs.Length * threshold > js.Length;

        }

        public static bool IsStringFontRef(string reference)
        {
            var fontTypes = FontTypes.GetFontTypes();
            return fontTypes.Any(font => reference.ToUpper().Contains(font));
        }
    }
}
