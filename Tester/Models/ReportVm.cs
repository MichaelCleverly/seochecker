﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SeoChecker.Models;

namespace Tester.Models
{
    public class ReportVm
    {
        public PageContent PageContent { get; set; }
        public Meta Meta { get; set; }
        public MobileOptimization MobileOptimization { get; set; }
        public Extras Extras { get; set; }
        public Security Security { get; set; }
        public SpeedOptimization SpeedOptimization { get; set; }
    }
}