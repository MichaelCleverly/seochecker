﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SeoChecker.Models;
using Tester.Models;
using SeoChecker = SeoChecker.SeoChecker;

namespace Tester.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        [OutputCache(Duration = 60)]
        public PartialViewResult ReportPartial(string url)
        {
            var seoChecker = new global::SeoChecker.SeoChecker(url);

            var vm = new ReportVm();

            try
            {
                vm.SpeedOptimization = seoChecker.GetSpeedOptimization();
                vm.PageContent = seoChecker.GetPageContents();
                vm.Extras = seoChecker.GetExtras();
                vm.Security = seoChecker.GetSecurity();
                vm.Meta = seoChecker.GetMeta();
                vm.MobileOptimization = seoChecker.GetMobileOptimization();
            }
            catch (Exception e)
            {
                var test = "";
            }

            return PartialView("Partials/_Report", vm);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}