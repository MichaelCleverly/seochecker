﻿
function doAjaxPost(url, data, successcallback, errorcallback, completecallback) {
    $.ajaxSetup({ cache: false });
    var jsonData = JSON.stringify(data);
    var parameters = {
        url: url,
        type: "POST",
        data: jsonData,
        contentType: 'application/json',
        success: function (data) {
            if (successcallback != undefined && successcallback != null) {
                successcallback(data);
            }
        },
        error: function (data) {
            if (errorcallback != undefined && errorcallback != null) {
                errorcallback(data);
            }
        },
        complete: function (data) {
            if (completecallback != undefined && completecallback != null) {
                completecallback(data);
            }
        },
    };

    $.ajax(parameters);
}

function doAjaxFormPost(url, form, successcallback, errorcallback, completecallback) {
    $.ajaxSetup({ cache: false });
    var parameters = {
        url: url,
        type: "POST",
        data: form,
        success: function (data) {
            if (successcallback != undefined && successcallback != null) {
                successcallback(data);
            }
        },
        error: function (data) {
            if (errorcallback != undefined && errorcallback != null) {
                errorcallback(data);
            }
        },
        complete: function (data) {
            if (completecallback != undefined && completecallback != null) {
                completecallback(data);
            }
        },
    };

    $.ajax(parameters);
}

function doAjaxFormPostWithFile(url, form, successcallback, errorcallback, completecallback) {
    $.ajaxSetup({ cache: false });
    var parameters = {
        url: url,
        type: "POST",
        data: form,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (successcallback != undefined && successcallback != null) {
                successcallback(data);
            }
        },
        error: function (data) {
            if (errorcallback != undefined && errorcallback != null) {
                errorcallback(data);
            }
        },
        complete: function (data) {
            if (completecallback != undefined && completecallback != null) {
                completecallback(data);
            }
        },
    };

    $.ajax(parameters);
}

function doAjaxGetWithData(url, data, successcallback, errorcallback, completecallback) {
    $.ajaxSetup({ cache: false });
    var parameters = {
        url: url,
        type: "GET",
        data: data,
        traditional: true,
        contentType: 'application/json',
        success: function (data) {
            if (successcallback != undefined && successcallback != null) {
                successcallback(data);
            }
        },
        error: function (data) {
            if (errorcallback != undefined && errorcallback != null) {
                errorcallback(data);
            } else {
                if (data.statusCode == 401) {
                    window.location.replace("/login");
                }
            }
        },
        complete: function (data) {
            if (completecallback != undefined && completecallback != null) {
                completecallback(data);
            }
        }
    };

    $.ajax(parameters);
}

function doAjaxGet(url, successcallback, errorcallback, completecallback) {
    $.ajaxSetup({ cache: false });
    var parameters = {
        url: url,
        type: "GET",
        contentType: 'application/json',
        success: function (data) {
            if (successcallback != undefined && successcallback != null) {
                successcallback(data);
            }
        },
        error: function (data) {
            if (errorcallback != undefined && errorcallback != null) {
                errorcallback(data);
            } else {
                if (data.statusCode == 401) {
                    window.location.replace("/login");
                }
            }
        },
        complete: function (data) {
            if (completecallback != undefined && completecallback != null) {
                completecallback(data);
            }
        }
    };

    $.ajax(parameters);
}

function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}